" Environment and flags
source $HOME/.config/nvim/flags.vimrc
" Configure basic mappings
source $HOME/.config/nvim/keys.vimrc
" Install Plugins
source $HOME/.config/nvim/init.vimrc
" Searching and completion configuration
source $HOME/.config/nvim/search.vimrc
" Language Server Protocol config
source $HOME/.config/nvim/lsp.vimrc
" Fixers - linters & formatters & test support
source $HOME/.config/nvim/fixers.vimrc
" Markdown support
source $HOME/.config/nvim/md.vimrc
" Javascript support
source $HOME/.config/nvim/js.vimrc
" lots of :set here
source $HOME/.config/nvim/general.vimrc
" Configure status line
source $HOME/.config/nvim/status-line.vimrc
" Theme configuration
source $HOME/.config/nvim/themes.vimrc
