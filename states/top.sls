base:
  '*':
    - fonts
    - brew
    - git
    - zsh
    - node
    - nvim
    - tmux
    - elixir
    - alacritty
    - task-warrior
    - ranger
